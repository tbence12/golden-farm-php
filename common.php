<?php

function loadUsers() {
    $file = fopen("felhasznalok.txt", "r");
    $felhasznalok = [];
    while (($line = fgets($file)) !== false) {
        $felhasznalok[] = unserialize($line);
    }
    fclose($file);
    return $felhasznalok;
}

function saveUser($user) {
    $file = fopen("felhasznalok.txt", "a");
    fwrite($file, serialize($user)."\n");
    fclose($file);
}

function loadGoldies() {
    $file = fopen("goldies.txt", "r");
    $goldies = [];
    while (($line = fgets($file)) !== false) {
        $goldies[] = unserialize($line);
    }
    fclose($file);
    return $goldies;
}

function saveGoldies($goldies) {
    $file = fopen("goldies.txt", "w");
    foreach ($goldies as $goldi) {
        fwrite($file, serialize($goldi)."\n");
    }
    fclose($file);
}

function saveGoldi($goldi) {
    $goldies = loadGoldies();
    array_unshift($goldies , $goldi);
    saveGoldies($goldies);
}
?>