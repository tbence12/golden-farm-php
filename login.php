<?php
session_start();
include("common.php");
$felhasznalok = loadUsers();
$uzenet = "";
if (isset($_POST["login"])) {
    $username = $_POST["username"];
    $password = $_POST["password"];

    $uzenet = "Sikertelen belépés";
    foreach($felhasznalok as $f) {
        if ($f["username"] == $username && $f["password"] == $password) {
            $uzenet = "Sikeres belépés";

            $_SESSION["felhasznalo"] = $f;

            break;
        }
    }
}
$username = "";
foreach($felhasznalok as $f) {
        $username = $f["username"]; 
    }
?>

<!DOCTYPE html>
<html lang="hu">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>Bejelentkezés</title>
	<link rel="icon" href="others/golden-icon.png" />
	<link rel="stylesheet" type="text/css" href="css/login.css" />
</head>
<body class="background">

	<header>
		Golden Farm
	</header>
	
	<div class="homehely">
    <a href="index.php">Home</a>
	</div>
	
	<div class="bejelentkezhely">
	<?php if (isset($_SESSION["felhasznalo"])) : ?>
		<a href="login.php"><?php echo $username; ?></a>
	<?php endif; ?>
	<?php if (!isset($_SESSION["felhasznalo"])) : ?>
		<a href="login.php">Bejelentkezés</a>
	<?php endif; ?>
	</div>
	
	<nav>
		<a class="link" href="golden-list.php">Goldi-lista</a>
		<a class="link" href="golden-igeny.php">Goldi-igénylés</a>
		<a class="link" href="golden-add.php">Goldi-hozzáadás</a>
	</nav>
	
	<?php if (!isset($_SESSION["felhasznalo"])) : ?>
	    <div id="reg"><a href="reg.php">Ha még nem regisztráltál katt ide</a></div>
	<?php endif; ?>
	<?php if (isset($_SESSION["felhasznalo"])) : ?>
	    <div id="reg"><a href="logout.php">Kijelentkezés</a></div>
	<?php endif; ?>
	
	<div class="uzenet">
	<?php
        echo $uzenet;
    ?>
	</div>
	<?php if (!isset($_SESSION["felhasznalo"])) : ?>
	<form action="login.php" method="post">
		<label for="felh">Felhasználónév:</label>
		<input id="felh" type="input" name="username" placeholder="Felhasználónév"/>
		
		<label for="passw">Jelszó:</label>
		<input id="passw" type="password" name="password" placeholder="Jelszó"/>
	  
		<button name="login" id="submitLogin">Bejelentkezés</button>  
	</form>
	<?php else : ?>
			<table>
				<tr>
                    <th>Név:</th>
                    <td><?php echo $_SESSION["felhasznalo"]["name"]; ?></td>
                </tr>
                <tr>
                    <th>Felhasználónév:</th>
                    <td><?php echo $_SESSION["felhasznalo"]["username"]; ?></td>
                </tr>
                <tr>
                    <th>E-mail:</th>
                    <td><?php echo $_SESSION["felhasznalo"]["email"]; ?></td>
                </tr>    
	        </table>
        <?php endif; ?>
    
	
</body>
</html>