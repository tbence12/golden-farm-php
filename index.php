<?php
session_start();
include("common.php");
$felhasznalok = loadUsers();
$uzenet = "";
$username = "";
foreach($felhasznalok as $f) {
        $username = $f["username"]; 
    }
?>
<!DOCTYPE html>
<html lang="hu">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>Golden Farm</title>
	<link rel="icon" href="others/golden-icon.png" />
	<link rel="stylesheet" type="text/css" href="css/index.css" />
</head>
<body class="background">
<video autoplay loop muted poster="css/index-background.jpg" src="others/golden-video.mp4"></video>
	<header>
		Golden Farm
	</header>
	
	<div class="homehely">
    <a href="index.php">Home</a>
	</div>
	
	<div class="bejelentkezhely">
	<?php if (isset($_SESSION["felhasznalo"])) : ?>
		<a href="login.php"><?php echo $username; ?></a>
	<?php endif; ?>
	<?php if (!isset($_SESSION["felhasznalo"])) : ?>
		<a href="login.php">Bejelentkezés</a>
	<?php endif; ?>
	</div>
	
	<nav>
		<a class="link" href="golden-list.php">Goldi-lista</a>
		<a class="link" href="golden-igeny.php">Goldi-igénylés</a>
		<a class="link" href="golden-add.php">Goldi-hozzáadás</a>
	</nav>
	
	<main>
        Üdvözlünk a Golden Farm oldalán ahol:
		 <ul>
			<li>Megnézheti milyen Goldik vannak nálunk</li>
			<li>Nálunk lévő Goldik közül igényelhet egyet <br> 
				(bejelentkezés szükséges)</li>
		</ul> 
    </main>
	
	<footer>
		&copy; TurB<sup>TM</sup> 2018
	</footer>
	
	  
</body>
</html>