<?php
session_start();
session_unset();
session_destroy();

include("common.php");
?>
<!DOCTYPE html>
<html lang="hu">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>Kijelentkezés</title>
	<link rel="icon" href="others/golden-icon.png" />
	<link rel="stylesheet" type="text/css" href="css/logout.css" />
</head>
<body class="background">

	<header>
		Golden Farm
	</header>
	
	<div class="homehely">
    <a href="index.php">Home</a>
	</div>
	
	<div class="bejelentkezhely">
    <a href="login.php">Bejelentkezés</a>
	</div>
	
	<nav>
		<a class="link" href="golden-list.php">Goldi-lista</a>
		<a class="link" href="golden-igeny.php">Goldi-igénylés</a>
		<a id="kiemelt" class="link" href="golden-add.php">Goldi-hozzáadás</a>
	</nav>

	<h1>Sikeres kijelentkezés</h1>
</body>
</html>