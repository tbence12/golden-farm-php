<?php
session_start();

include("common.php");

$felhasznalok = loadUsers();

$name = "";
$username = "";
$password = "";
$password2 = "";
$email = "";


if (isset($_POST["reg"])) {
	$name = $_POST["name"];
    $username = $_POST["username"];
    $password = $_POST["password"];
	$password2 = $_POST["password2"];
    $email = $_POST["email"];
}
?>

<!DOCTYPE html>
<html lang="hu">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>Regisztrálás</title>
	<link rel="icon" href="others/golden-icon.png" />
	<link rel="stylesheet" type="text/css" href="css/reg.css" />
</head>
<body class="background">

	<header>
		Golden Farm
	</header>
	
	<div class="homehely">
    <a href="index.php">Home</a>
	</div>
	
	<div class="bejelentkezhely">
    <a href="login.php">Bejelentkezés</a>
	</div>
	
	<nav>
		<a class="link" href="golden-list.php">Goldi-lista</a>
		<a class="link" href="golden-igeny.php">Goldi-igénylés</a>
		<a class="link" href="golden-add.php">Goldi-hozzáadás</a>
	</nav>
	
	<form action="reg.php" method="post">
		<label for="nev">Neved:</label>
		<input id="nev" type="input" name="name" value="<?php echo $name;?>" placeholder="Neved"/>
	
		<label for="felh">Felhasználónév:</label>
		<input id="felh" type="input" name="username" value="<?php echo $username;?>" placeholder="Felhasználónév"/>
		
		<label for="passw">Jelszó:</label>
		<input id="passw" type="password" name="password" placeholder="Jelszó"/>
		
		<label for="passw2">Jelszó mégegyszer:</label>
		<input id="passw2" type="password" name="password2" placeholder="Jelszó mégegyszer"/>
		
		<label for="email">E-mail címed:</label>
		<input id="email" type="input" name="email " value="<?php echo $email;?>" placeholder="E-mail címed"/>
	  
		<button id="submitReg" name="reg">Regisztrálás</button>  
	</form>
	<?php
	if (isset($_POST["reg"])) {
    $errors = [];
    foreach($felhasznalok as $f) {
        if ($f["username"] == $username) {
            $errors[] = "Foglalt felhasználónév.";
            break;
        }
    }

    if (strlen($password) < 3) {
        $errors[] = "Legalább 3 karakter hosszúnak kell lennie a jelszónak.";
    } elseif (!(preg_match( '~[A-Za-z]~', $password) && preg_match( '~\d~', $password))) {
        $errors[] = "Legalább 1 karaktert és 1 számot tartalmaznia kell a jelszónak.";
    } elseif ($password != $password2) {
        $errors[] = "Nem egyezik meg a két jelszó.";
    }

    if (sizeof($errors) == 0) {
        echo "Sikeres regisztráció";

        saveUser([
			"name" => $name,
            "username" => $username,
            "password" => $password,
            "email" => $email,
        ]);
		header("Location: login.php");
        exit;
    } else {
        foreach ($errors as $error) {
            echo $error."<br>";
        }
    }
}

        ?>
	
</body>
</html>