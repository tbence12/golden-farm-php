<?php
session_start();
include("common.php");
$felhasznalok = loadUsers();
$uzenet = "";
if (!isset($_SESSION["felhasznalo"])) {
    header("Location: login.php");
    // Ki kell lépni a programból, hogy a többi rész ne fusson le
    exit;
}
$goldiid = "";
$name = "";
$age = "";
$ability = "";
$errors = [];

$username = "";
foreach($felhasznalok as $f) {
        $username = $f["username"]; 
    }
if (isset($_POST["add"])) {
    $goldiid = $_POST["goldiid"];
    $name = $_POST["name"];
    $age = $_POST["age"];
	$ability = $_POST["ability"];

    if (strlen($name) < 4) {
        $errors[] = "Legalább 4 karakter hosszúnak kell lennie a névnek.";
    }
    $pic = "";
    // Ellenőrizzük, hogy van-e feltöltött kép
    if (isset($_FILES["image"]) && $_FILES["image"]["tmp_name"] != "") {
        // 2 MB-nál nagyobb fájlt nem engedünk meg
        if ($_FILES["image"]["size"] > 2097152) {
            $errors[] = "Legfeljebb 2 MB-os kép lehet.";
        }
        $info = getimagesize($_FILES["image"]["tmp_name"]);
        $mime = $info["mime"];
        // Csak jpg vagy png képeket fogadunk el
        if ($mime != "image/jpeg" && $mime != "image/png") {
            $errors[] = "Csak jpeg/png.";
        }
        // Az útvonal, ahova mentjük a fájlt
        // Ha a feltöltött fájlok neve azonos, akkor felülírják egymást!
        // Feladat: egyedi fájlnév generálása (pl: time+hash)
		$pic = "img/".basename($goldiid);

    }

    if (sizeof($errors) == 0) {
        
        if ($pic != "") {
            // Feltöltött fájl átmásolása
            move_uploaded_file(
                $_FILES["image"]["tmp_name"],
                $pic
            );
        }
        
        saveGoldi([
            "goldiid" => $goldiid,
            "name" => $name,
            "age" => $age,
            "ability" => $ability,
            "kep" => $pic,
            "felhasznalo" => $_SESSION["felhasznalo"]["username"], // Belépett felhasználó lementése a bejegyzéshez
        ]);

        // Sikeres mentés után átirányítás a bejegyzések listázására
        header("Location: golden-list.php");
        exit;

    }
	

}
?>
<!DOCTYPE html>
<html lang="hu">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>Goldi-hozzáadás</title>
	<link rel="icon" href="others/golden-icon.png" />
	<link rel="stylesheet" type="text/css" href="css/golden-add.css" />
</head>
<body class="background">

	<<header>
		Golden Farm
	</header>
	
	<div class="homehely">
    <a href="index.php">Home</a>
	</div>
	
	<div class="bejelentkezhely">
	<?php if (isset($_SESSION["felhasznalo"])) : ?>
		<a href="login.php"><?php echo $username; ?></a>
	<?php endif; ?>
	<?php if (!isset($_SESSION["felhasznalo"])) : ?>
		<a href="login.php">Bejelentkezés</a>
	<?php endif; ?>
	</div>
	
	<nav>
		<a class="link" href="golden-list.php">Goldi-lista</a>
		<a class="link" href="golden-igeny.php">Goldi-igénylés</a>
		<a id="kiemelt" class="link" href="golden-add.php">Goldi-hozzáadás</a>
	</nav>

	<form action="golden-add.php" method="post" enctype="multipart/form-data">
		<label for="goldiID">Goldi ID:</label>
		<input id="goldiID" type="input" name="goldiid" value="<?php echo $goldiid;?>" placeholder="Goldi ID"/>
		
		<label for="nev">Goldi neve:</label>
		<input id="nev" type="input" name="name" value="<?php echo $name;?>" placeholder="Goldi neve"/>
		
		<label for="kor">Kor(év):</label>
		<input id="kor" type="input" name="age" value="<?php echo $age;?>" placeholder="Kor"/>
		
		<label for="kepesseg">Képessége:</label>
		<input id="kepesseg" type="input" name="ability" value="<?php echo $ability;?>" placeholder="Képesség"/>
			
		<label for="kep">Kép:</label>
		<input id="kep" type="file" name="pic" value="<?php echo $pic;?>" accept="image/*">
	  
		<button id="submitAdd" name="add">Hozzáadás</button>  
		<?php
                foreach ($errors as $error) {
                    echo $error."<br>";
                }
            ?>
	</form>
</body>
</html>