<?php
session_start();
include("common.php");
$felhasznalok = loadUsers();
$uzenet = "";
$goldik = loadGoldies();
$username = "";
foreach($felhasznalok as $f) {
        $username = $f["username"]; 
    }
?>
<!DOCTYPE html>
<html lang="hu">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>Goldi-lista</title>
	<link rel="icon" href="others/golden-icon.png" />
	<link rel="stylesheet" type="text/css" href="css/golden-list.css" />
</head>
<body class="background">

	<header>
		Golden Farm
	</header>
	
	<div class="homehely">
    <a href="index.php">Home</a>
	</div>
	
	<div class="bejelentkezhely">
	<?php if (isset($_SESSION["felhasznalo"])) : ?>
		<a href="login.php"><?php echo $username; ?></a>
	<?php endif; ?>
	<?php if (!isset($_SESSION["felhasznalo"])) : ?>
		<a href="login.php">Bejelentkezés</a>
	<?php endif; ?>
	</div>
	
	<nav>
		<a id="kiemelt" class="link" href="golden-list.php">Goldi-lista</a>
		<a class="link" href="golden-igeny.php">Goldi-igénylés</a>
		<a class="link" href="golden-add.php">Goldi-hozzáadás</a>
	</nav>

	<div id="table-overflow">
      <table>
  <!--    <caption>Goldi-lista</caption>-->
        <thead>
          <tr>
            <th>Goldi ID</th>
            <th>Név</th>
            <th>Kor(év)</th>
            <th>Képességek</th>
            <th>Kép</th>
          </tr>
        </thead>
        <tbody>
		<?php foreach ($goldik as $b) : ?>
          <tr>
            <td><?php echo $b["goldiid"]; ?></td>
            <td><?php echo $b["name"]; ?></td>
            <td><?php echo $b["age"]; ?></td>
            <td><?php echo $b["ability"]; ?></td>
			<td><img src="<?php if (isset($b["kep"]) && $b["kep"] != "") echo $b["kep"]; else echo "img/1.jpg" ?>" height="200" width="350"></td>
          </tr>
		 <?php endforeach; ?>
	<!--	  <tr>
            <td>#2</td>
            <td>Sánta</td>
            <td>7</td>
            <td>Fekszik<br>Lábatfelemel<br>Fut</td>
			<td><img src="img/2.jpg" height="200" width="350"></td>
          </tr>
		  <tr>
            <td>#3</td>
            <td>Haver</td>
            <td>4</td>
            <td>Pacsi</td>
			<td><img src="img/3.jpg" height="200" width="350"></td>
          </tr>
		  <tr>
            <td>#4</td>
            <td>Sanyi</td>
            <td>1</td>
            <td>Labdát visszahoz</td>
			<td><img src="img/4.jpg" height="200" width="350"></td>
          </tr>
		  <tr>
            <td>#5</td>
            <td>Lusti</td>
            <td>3</td>
            <td>Fekszik<br>Ugat</td>
			<td><img src="img/5.jpg" height="200" width="350"></td>
          </tr>-->
        </tbody>
      </table>
    </div>
	
	  
</body>
</html>